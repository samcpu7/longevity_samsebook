import 'package:flutter/material.dart';

class TopBar extends StatelessWidget {
  const TopBar({
    Key? key,
    this.showLeftArrow = false,
    required this.title,
    this.rightWidget,
  }) : super(key: key);

  final bool showLeftArrow;
  final String? title;
  final Widget? rightWidget;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
            child: Align(
                alignment: Alignment.centerLeft,
                child: showLeftArrow
                    ? IconButton(
                        icon: const Icon(Icons.arrow_back,
                            color: Color(0xFF150B3D)),
                        onPressed: () {
                          Navigator.pop(context);
                        },
                      )
                    : Container())),
        Expanded(
            flex: 2,
            child: Center(
              child: Text(title!,
                  style: const TextStyle(
                      fontSize: 18,
                      color: Color(0xFF150B3D),
                      fontWeight: FontWeight.bold)),
            )),
        Expanded(
            child: Align(
                alignment: Alignment.centerRight,
                child: Padding(
                  padding: const EdgeInsets.only(right: 18.0),
                  child: rightWidget,
                )))
      ],
    );
  }
}
