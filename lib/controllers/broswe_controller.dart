import 'dart:async';
import 'dart:collection';
import 'dart:io';

import 'package:external_path/external_path.dart';
import 'package:flutter/material.dart';

class BrowseController extends ChangeNotifier {
  BrowseController();

  bool refreshing = false;
  Map<String, List<File>> items = {};
  String mimeType = '.pdf';

  Map get sortFiles{
    return  SplayTreeMap<String, List<File>>.from(items, (keys1, keys2) => keys1.compareTo(keys2));
  }

  queryResult(context) async {
    refreshing = true;
    notifyListeners();

    List<String> paths = await getPaths();
    print(paths.toString());
    items={};

    for (int i = 0; i < paths.length; i++) {
      await queryPath(paths[i]);
    }

    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text("files number of order: ${items.length}"),
    ));
    
    refreshing = false;
    notifyListeners();
  }

  queryPath(path) async {
    final rootDir = Directory(path);

    Stream<FileSystemEntity> _stream = rootDir.list(recursive: true);
    Completer<void> _complete = Completer();
    _stream.listen((event) {
      
      if (event is File && !isHidden(event.path) && validType(event.path)) {
        addFile(getFistLetter(event), event);
      }
      }).onDone(() {
      _complete.complete();
    });
    return _complete.future;
  }

  String getFistLetter(File file){
    List<String> names=file.path.split("/");
    return names.last.substring(0, 1).toUpperCase();
  }

  bool validType(String path) => path.contains(mimeType);

  bool isHidden(String filePath) => filePath.startsWith('.');

  Future<List<String>> getPaths() async {
    var paths = await ExternalPath.getExternalStorageDirectories();
    print(paths);
    return paths;
  }

  void addFile(String key, File file) {
    items.update(
      key,
      (files){
        files.add(file);
        return files;
      },
      ifAbsent: () { 
        print("Key: $key");
        List<File> files=[file]; 
        return files;
      },
    );
  }
  
}
