import 'dart:convert';
import 'dart:io';
import 'package:sam/database/database.dart';
import 'package:sam/database/session_db.dart';
import 'package:sam/models/book.dart';
import 'package:sqflite/sqflite.dart';

class BookDB {
  Future<int?> insert(Book book) async {
    // print(book.toJson());
    int? id;
    final Database? db = await DBProvider.database;
    await db!.transaction((txn) async {
      id=await txn.insert(
        'books',
        book.toJson(),
        conflictAlgorithm: ConflictAlgorithm.replace,
      );
    });
    return id;
  }

  Future<Book?> getBookById(int id) async {
    final db = await DBProvider.database;
    var result =
        await db!.query("books", where: "id = ?", whereArgs: [id]);
    Book? book =
        result.isNotEmpty ? Book.fromJson(result.first) : null;
    if(book !=null){
      book.lastSession=await SessionDB().lastSession(book.id!);
    }
    return book;
  }

  Future<bool> isInLibrary(File file) async {
    final db = await DBProvider.database;
    var result =
        await db!.query("books", where: "path = ?", whereArgs: [file.path]);
    return 
        result.isNotEmpty ? true : false;
  }

  Future<List<Book>> books() async {
    final Database? db = await DBProvider.database;

    final List<Map<String, dynamic>> maps =
        await db!.query('books', orderBy: "title ASC");

    List<Book> books = [];
    for (int i = 0; i < maps.length; i++) {
      Book book = Book.fromJson(maps[i]);
      book.lastSession=await SessionDB().lastSession(book.id!);
      books.add(book);
    }
    return books;
  }

  Future<List<Book>> otherBooks() async {
    final Database? db = await DBProvider.database;

    final List<Map<String, dynamic>> maps =
        await db!.query('books', orderBy: "created_at DESC", limit: 5);

    List<Book> books = [];
    for (int i = 0; i < maps.length; i++) {
      Book book = Book.fromJson(maps[i]);
      book.lastSession=await SessionDB().lastSession(book.id!);
      books.add(book);
    }
    return books;
  }

  Future<Book?> lastBook() async {
    final Database? db = await DBProvider.database;

    var result = await db!.query('books',
        orderBy: "updated_at DESC",
        limit: 1);

    Book? book= result.isNotEmpty ? Book.fromJson(result.last) : null;
    if(book !=null){
      book.lastSession=await SessionDB().lastSession(book.id!);
      if(book.lastSession==null){
        return null;
      }
    }

    return book;
  }

  Future<void> update(Book book) async {
    // ignore: unused_local_variable
    final db = await DBProvider.database;

    await db!.update(
      'books',
      book.toJson(),
      where: "id = ?",
      whereArgs: [book.id],
    );
  }

  delete(int id) async {
    final db = await DBProvider.database;
    db!.delete(
      'books',
      where: "id = ?",
      whereArgs: [id],
    );
  }
}

