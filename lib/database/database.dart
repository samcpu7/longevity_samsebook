
import 'dart:io';
import 'package:path/path.dart' show join;
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
// ignore: unused_import
import 'package:flutter/foundation.dart' show kIsWeb;
class DBProvider {
  DBProvider._();
  static final DBProvider db = DBProvider._();
  static Database? _database;
  static Future<Database?> get database async {
    if (_database != null) return _database;
    _database = await initDB();
    return _database;
  }
  static initDB() async {
    String name = "ebook.3.db";
    print(name);
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, name);
    return await openDatabase(path, version: 32, onOpen: (db) {},
        onCreate: (Database db, int version) async {
      await db.execute("CREATE TABLE books ("
          "id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
          "title TEXT,"
          "path TEXT,"
          "created_at TEXT,"
          "updated_at TEXT"
          ")");

      await db.execute("CREATE TABLE sessions ("
          "id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
          "book_id INTEGER NOT NULL,"
          "started_at TEXT NOT NULL,"
          "page_number INTEGER NOT NULL,"
          "created_at TEXT NULL DEFAULT NULL,"
          "updated_at TEXT NULL DEFAULT NULL"
          ")");
      
    }, );
  }
}