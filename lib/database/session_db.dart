import 'dart:convert';
import 'package:sam/database/database.dart';
import 'package:sam/models/session.dart';
import 'package:sqflite/sqflite.dart';

class SessionDB {
  Future<int?> insert(Session session) async {
    print(session.toJson());
    int? id;
    final Database? db = await DBProvider.database;
    await db!.transaction((txn) async {
      id=await txn.insert(
        'sessions',
        session.toJson(),
        conflictAlgorithm: ConflictAlgorithm.replace,
      );
    });

    return id;
  }

  Future<Session?> getSessionById(int id) async {
    final db = await DBProvider.database;
    var result =
        await db!.query("sessions", where: "id = ?", whereArgs: [id]);
    Session? session =
        result.isNotEmpty ? Session.fromJson(result.first) : null;

    return session;
  }

  Future<void> insertAll(List<Session> sessions) async {
    final Database? db = await DBProvider.database;
    Batch batch = db!.batch();
    sessions.forEach((element) {
      // print(element.toJson());
      batch.insert(
        'sessions',
        element.toJson(),
        conflictAlgorithm: ConflictAlgorithm.replace,
      );
    });

    await batch.commit(noResult: true);
  }

  Future<List<Session>> sessions() async {
    final Database? db = await DBProvider.database;

    final List<Map<String, dynamic>> maps =
        await db!.query('sessions', orderBy: "updated_at DESC");

    print(maps);
    print(">>>>>>>>>>");
    List<Session> sessions = [];
    for (int i = 0; i < maps.length; i++) {
      Session session = Session.fromJson(maps[i]);
      sessions.add(session);
    }
    return sessions;
  }

  Future<Session?> lastSession(int bookId) async {
    final Database? db = await DBProvider.database;

    var result = await db!.query('sessions',
        orderBy: "updated_at DESC",
        where: "book_id = ?",
        whereArgs: [bookId],
        limit: 1);
    print(result);
    return result.isNotEmpty ? Session.fromJson(result.last) : null;
  }

  Future<void> update(Session session) async {
    final db = await DBProvider.database;
    print("session id = ${session.id}");
    await db!.update(
      'sessions',
      session.toJson(),
      where: "id = ?",
      whereArgs: [session.id],conflictAlgorithm: ConflictAlgorithm.replace,
    );

    print("=================");
    print(session.toJson());
    print("=================");
  }

  delete(int id) async {
    final db = await DBProvider.database;
    db!.delete(
      'sessions',
      where: "id = ?",
      whereArgs: [id],
    );
  }
}

