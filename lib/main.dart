import 'package:flutlab_logcat/flutlab_logcat.dart';
import 'package:flutter/material.dart';
import 'package:sam/views/main_view.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  // await FlutLabLogcat.init();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      // Application name
      title: 'Sams Ebook',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MainView(),
    );
  }
}
