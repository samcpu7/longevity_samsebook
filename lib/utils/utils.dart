import 'dart:io';

import 'package:file_picker/file_picker.dart';

Future<File?> pickFile() async {
  FilePickerResult? result = await FilePicker.platform.pickFiles(
    type: FileType.custom,
    allowedExtensions: ['pdf'],
  );

  if (result != null) {
    File file = File(result.files.single.path!);
    return file;
  } else {
    // User canceled the picker
    return null;
  }
}

getFileName(File file) {
    List<String> arr = file.path.split("/");
    return arr[arr.length - 1];
  }
