import 'dart:convert';

Session sessionFromJson(String str) => Session.fromJson(json.decode(str));

String sessionToJson(Session data) => json.encode(data.toJson());

class Session {
    Session({
        this.id,
        this.bookId,
        this.startedAt,
        this.pageNumber,
        this.createdAt,
        this.updatedAt,
    });

    int? id;
    int? bookId;
    String? startedAt;
    int? pageNumber;
    String? createdAt;
    String? updatedAt;

    factory Session.fromJson(Map<String, dynamic> json) => Session(
        id: json["id"],
        bookId: json["book_id"],
        startedAt: json["started_at"],
        pageNumber: json["page_number"],
        createdAt: json["created_at"],
        updatedAt: json["updated_at"],
    );

    Map<String, dynamic> toJson() => {
        "book_id": bookId,
        "started_at": startedAt,
        "page_number": pageNumber,
        "created_at": createdAt,
        "updated_at": updatedAt,
    };
}
