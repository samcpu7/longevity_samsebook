
import 'dart:convert';

import 'package:sam/models/session.dart';

Book bookFromJson(String str) => Book.fromJson(json.decode(str));

String bookToJson(Book data) => json.encode(data.toJson());

class Book {
    Book({
        this.id,
        this.title,
        this.path,
        this.createdAt,
        this.updatedAt,
    });

    int? id;
    String? title;
    String? path;
    String? createdAt;
    String? updatedAt;
    Session? lastSession;

    factory Book.fromJson(Map<String, dynamic> json) => Book(
        id: json["id"],
        title: json["title"],
        path: json["path"],
        createdAt: json["created_at"],
        updatedAt: json["updated_at"],
    );

    Map<String, dynamic> toJson() => {
        "title": title,
        "path": path,
        "created_at": createdAt,
        "updated_at": updatedAt,
    };
}
