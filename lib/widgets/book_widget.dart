import 'dart:io';

import 'package:flutter/material.dart';
import 'package:pdfx/pdfx.dart';
import 'package:sam/models/book.dart';

class BookWidget extends StatefulWidget {
  const BookWidget({Key? key, required this.book}) : super(key: key);
  final Book book;
  @override
  _BookWidgetState createState() => _BookWidgetState();
}

class _BookWidgetState extends State<BookWidget> {
  late PdfDocument document;
  double width = 120;
  double height = 200;
  PdfPageImage? pdfImage;

  @override
  void initState() {
    super.initState();

    openDocument();
  }

  openDocument() async {
    document = await PdfDocument.openFile(widget.book.path!);
    PdfPage cover = await document.getPage(1);
    pdfImage = await cover.render(width: width, height: height);
    if (pdfImage != null) {
      setState(() {
        
      });
    }
  }

  @override
  setState(function){
    if(mounted){
      super.setState(function);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(4),
      height: height+21,
      child: Column(children: [
        Expanded(
          child: Container(
            width: width,
            height: height,
            decoration: BoxDecoration(color: Colors.white, border: Border.all(color: Colors.black, width: 1),),
            child: pdfImage != null
                  ? Image.memory(pdfImage!.bytes)
                  : const Center(
                      child: CircularProgressIndicator(),
                    )),
        ),
        const SizedBox(height: 5),
        SizedBox(width: width+20, child: Center(child: Text(widget.book.title!, overflow: TextOverflow.ellipsis,)))
      ]),
    );
  }
}
