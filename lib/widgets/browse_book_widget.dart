import 'dart:io';

import 'package:flutter/material.dart';
import 'package:pdfx/pdfx.dart';
import 'package:sam/database/books_db.dart';
import 'package:sam/models/book.dart';
import 'package:sam/utils/utils.dart';

class BrowseBookWidget extends StatefulWidget {
  const BrowseBookWidget({Key? key, required this.file}) : super(key: key);
  final File file;
  @override
  _BrowseBookWidgetState createState() => _BrowseBookWidgetState();
}

class _BrowseBookWidgetState extends State<BrowseBookWidget> {
  late PdfDocument document;
  double width = 100;
  double height = 150;
  PdfPageImage? pdfImage;
  bool isInLibrary=true;

  @override
  void initState() {
    super.initState();

    openDocument();
  }

  openDocument() async {
    isInLibrary=await BookDB().isInLibrary(widget.file);
    document = await PdfDocument.openFile(widget.file.path);
    PdfPage cover = await document.getPage(1);
    pdfImage = await cover.render(width: width, height: height);
    if (pdfImage != null) {
      setState(() {
        
      });
    }
  }

  @override
  setState(function){
    if(mounted){
      super.setState(function);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(4),
      height: height+21,
      child: Column(children: [
        Expanded(
          child: Container(
            width: width,
            height: height,
            decoration: BoxDecoration(color: Colors.white, border: Border.all(color: Colors.black, width: 1),),
            child: pdfImage != null
                  ? Image.memory(pdfImage!.bytes)
                  : const Center(
                      child: CircularProgressIndicator(),
                    )),
        ),
        const SizedBox(height: 5),
        SizedBox(width: width+20, child: Text(getFileName(widget.file), overflow: TextOverflow.ellipsis,)),
        if(!isInLibrary)
        Padding(
          padding: const EdgeInsets.all(2.0),
          child: ElevatedButton(onPressed: ()async{
            String title=getFileName(widget.file);
            Book book=Book(title: title, path: widget.file.path,createdAt: DateTime.now().toString(), updatedAt: DateTime.now().toString());
            await BookDB().insert(book);
            ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("added ${getFileName(widget.file)} to library")));
            isInLibrary=await BookDB().isInLibrary(widget.file);
            setState(() { });
          }, child: const Text("Add to library")),
        )
      ]),
    );
  }

  
}
