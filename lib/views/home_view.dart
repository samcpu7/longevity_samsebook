import 'dart:io';

import 'package:flutter/material.dart';
import 'package:sam/components/topbar.dart';
import 'package:sam/database/books_db.dart';
import 'package:sam/database/session_db.dart';
import 'package:sam/models/book.dart';
import 'package:sam/models/session.dart';
import 'package:sam/utils/utils.dart';
import 'package:sam/views/ebook_view.dart';
import 'package:sam/widgets/book_widget.dart';

class HomeView extends StatefulWidget {
  const HomeView({Key? key}) : super(key: key);
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  Book? lastBook;

  @override
  initState(){
    super.initState();
    BookDB().lastBook().then((value) {
      setState(() {
        lastBook=value;
      });
    });

    SessionDB().sessions();
  }
  Future<Book?> getLastBook()async{
    return BookDB().lastBook();
  }
  
  Future<List<Book>> getOtherBooks()async{
    return BookDB().otherBooks();
  }

  reload()async{
    setState(() {
      
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: const Color(0xFFF5F7FB),
        body: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
            const SizedBox(
              height: 30,
            ),
            TopBar(
              title: "Home",
              rightWidget: GestureDetector(
                  onTap: () async {
                    
                    File? file = await pickFile();
                    if(file !=null){        
                      Book book = Book(title: getFileName(file), path: file.path, createdAt: DateTime.now().toString(), updatedAt: DateTime.now().toString());
                      int? id=await  BookDB().insert(book);         
                      book.id=id;  
                      setState(() {
                        
                      });
                      
                      Navigator.push(context, MaterialPageRoute(builder: (context){
                        return EBookView(book);
                      })).then((value) async {
                       await reload();
                      });
                    }
                  },
                  child: const Icon(Icons.library_add)),
            ),const Padding(
              padding: EdgeInsets.all(10.0),
              child: Text("Welcome.\nPlease select your last read book from below or try one of the recently added books below", textAlign: TextAlign.center,style: TextStyle(fontStyle: FontStyle.italic),),
            ),
            Expanded(child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox(height: 30,),
                  const Text("Last Read", style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)), 
                  FutureBuilder<Book?>(
                    future: getLastBook(),
                    builder: (context, snapshot) {
                      switch(snapshot.connectionState){
                        
                        case ConnectionState.waiting:
                        case ConnectionState.active:
                          return const Center(child: CircularProgressIndicator());
                  
                        case ConnectionState.done:
                          lastBook=snapshot.data;
                          if(lastBook!=null){
                            return GestureDetector(child: BookWidget(book: lastBook!), onTap: (){
                            Navigator.push(context, MaterialPageRoute(builder: (context){
                                return EBookView(lastBook!);
                              }));
                            },);
                          }                            
                          else {
                            const SizedBox(height: 180,  child: Center(child: Text("You haven't read any book yet")));
                          }
                          break;
                          case ConnectionState.none:
                      
                          break;
                        }
                        return 
                            const SizedBox(height: 180,  child: Center(child: Text("nothing to show")));
                      },
                    
                    ),
                  
                  const SizedBox(height: 7,),
                  const Text("Recently Added Books", style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),),
                  FutureBuilder<List<Book>>(
                    future: getOtherBooks(),
                    builder: (context, snapshot){
                      switch(snapshot.connectionState){
                        case ConnectionState.waiting:
                        case ConnectionState.active:
                          return const Center(child: CircularProgressIndicator());
                        case ConnectionState.done:
                          if(snapshot.hasData){
                            List<Book> books=snapshot.data!;
                            if(books.isNotEmpty){
                            return SizedBox(
                              height: 200,
                              child: ListView.builder(
                              scrollDirection: Axis.horizontal,
                              shrinkWrap: true,
                              itemCount: books.length,
                              itemBuilder: (BuildContext context, int index) {  
                                return GestureDetector(child: BookWidget(book: books[index]), onTap: (){
                                  Navigator.push(context, MaterialPageRoute(builder: (context){
                                    return EBookView(books[index]);
                                  })).then((value) async {
                                    await reload();
                                  });
                                },);
                              },
                            ),);
                            }else{

                              return const SizedBox(height: 200, child: Center(child: Text("You do not have any books in your libray. Go to browse and find a book", textAlign: TextAlign.center, style: TextStyle(fontStyle: FontStyle.italic),),));
                            }
                          }else if(snapshot.hasError){
                            return const Center(child: Text("Something went wrong"),);
                          }
                        
                          break;
                        case ConnectionState.none:
                          break;
                      }
                  
                      return const SizedBox(height: 200, child: Center(child: Text("You do not have any books in your libray. Go to browse and find a book", textAlign: TextAlign.center,),));
                    }
                  )
                ]),
            ),
            ),
          ]),
        ),
      ),
    );
  }
}
