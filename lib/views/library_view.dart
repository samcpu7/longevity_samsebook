import 'dart:io';

import 'package:flutter/material.dart';
import 'package:sam/components/topbar.dart';
import 'package:sam/database/books_db.dart';
import 'package:sam/models/book.dart';
import 'package:sam/utils/utils.dart';
import 'package:sam/views/ebook_view.dart';
import 'package:sam/widgets/book_widget.dart';

class LibraryView extends StatefulWidget {
  const LibraryView({Key? key}) : super(key: key);
  _LibraryViewState createState() => _LibraryViewState();
}

class _LibraryViewState extends State<LibraryView> {

  @override
  initState(){
    super.initState();
  }
  
  Future<List<Book>> getBooks(){
    print("getting books from library");
    return BookDB().books();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: const Color(0xFFF5F7FB),
        body: Column(children: [
          const SizedBox(
            height: 30,
          ),
          TopBar(
            title: "Books",
            rightWidget: GestureDetector(
                onTap: () async {
                  
                  File? file = await pickFile();
                  if(file !=null){        
                    Book book = Book(title: getFileName(file), path: file.path, createdAt: DateTime.now().toString(), updatedAt: DateTime.now().toString());
                    int? id=await  BookDB().insert(book);         
                    book.id=id;           
                    setState(() {
                      
                    });
                    
                    Navigator.push(context, MaterialPageRoute(builder: (context){
                      return EBookView(book);
                    }));
                  }
                },
                child: const Icon(Icons.library_add)),
          ),
          const Padding(
            padding: EdgeInsets.all(18.0),
            child: Text("Please select a book below to read", textAlign: TextAlign.center,),
          ),
          Expanded(
            child: FutureBuilder<List<Book>>(
            future: getBooks(),
            builder: (context, snapshot){
              switch(snapshot.connectionState){
                case ConnectionState.waiting:
                case ConnectionState.active:
                  return const Center(child: CircularProgressIndicator());
                case ConnectionState.done:
                  if(snapshot.hasData){
                    List<Book> books=snapshot.data!;
                    if(books.isEmpty){
                      return const Padding(
                        padding: EdgeInsets.all(28.0),
                        child: Center(child: Text("You do not have any books in your libray. Go to browse and find a book", textAlign: TextAlign.center,style: TextStyle(fontStyle: FontStyle.italic))),
                      );
                    }
                    return GridView.builder(itemCount: books.length, shrinkWrap: true, itemBuilder: (context, index){
                      return GestureDetector(child: BookWidget(book: books[index]), onTap: (){
                        Navigator.push(context, MaterialPageRoute(builder: (context){
                                return EBookView(books[index]);
                            })).then((value) async {
                                setState(() {
                                  
                                });
                            });
                      },);
                    }, gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2, ),);
                  }else if(snapshot.hasError){
                    return const Center(child: Text("Something went wrong"),);
                  }
                 
                  break;
                case ConnectionState.none:
                  break;
              }
              
              return const Padding(
                padding: EdgeInsets.all(28.0),
                child: Center(child: Text("You do not have any books in your libray. Go to browse and find a book", textAlign: TextAlign.center,style: TextStyle(fontStyle: FontStyle.italic))),
              );
            },
          
          )),
        ]),
      ),
    );
  }
}
