import 'dart:io';

import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:sam/components/topbar.dart';
import 'package:sam/controllers/broswe_controller.dart';
import 'package:sam/widgets/browse_book_widget.dart';

class BrowseView extends StatefulWidget {
  const BrowseView({Key? key, required this.controller}) : super(key: key);

  final BrowseController controller;

  _BrowseViewState createState() => _BrowseViewState();
}

class _BrowseViewState extends State<BrowseView> {
  late BrowseController controller;
  @override
  void initState() {
    controller = widget.controller;
    
    super.initState();
  }

  findBooks()async{
    if(controller.refreshing)return;
          print('finding books');

    PermissionStatus permission = await Permission.storage.status;
                  if (permission != PermissionStatus.granted) {
                    await Permission.storage.request();
                    PermissionStatus permission = await Permission.storage.status;
                    if (permission != PermissionStatus.granted) {
                      return;
                    }
                  }

                  await controller.queryResult(context);
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: const Color(0xFFF5F7FB),
        body: Column(children: [
          const SizedBox(
            height: 30,
          ),
          TopBar(
            title: "Browse Books",
            rightWidget: GestureDetector(
                onTap: () async {
                  
                  
                  await findBooks();
                },
                child: const Icon(Icons.refresh)),
          ),
          Expanded(child: BookListView(controller: controller, findBooks: findBooks,)),
          if (controller.refreshing) const LinearProgressIndicator(),
        ]),
      ),
    );
  }


}

class BookListView extends StatefulWidget {
  BookListView({Key? key, required this.controller, required this.findBooks }) : super(key: key);
  final BrowseController controller;
  Function findBooks;

  _BookListViewState createState() => _BookListViewState();
}

class _BookListViewState extends State<BookListView> {
  Map<String, List<File>> files = {};
  
  @override
  initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return widget.controller.items.isEmpty?getNoItemScreen(widget.findBooks): Container(child: SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          for(final entry in widget.controller.sortFiles.entries)
            getLetterRow(entry)
        ],
      ),
    ));
  }

  getLetterRow(MapEntry entry){
    String letter=entry.key;
    List<File> files=entry.value;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
      Text(letter, style: const TextStyle(fontSize: 21, fontWeight: FontWeight.bold),),
      SizedBox(
        height: 170,
        child: ListView.builder(
        scrollDirection: Axis.horizontal,
        shrinkWrap: true,
        itemCount: files.length,
        itemBuilder: (BuildContext context, int index) {  
          return BrowseBookWidget(file: files[index]);
        },
      ),),
      
    ]);
  }

  getNoItemScreen(findBooks){
    return Column(mainAxisAlignment: MainAxisAlignment.center, children: [
      const Text("No books displayed."),
      const Text("Click below to find books on device"),
      ElevatedButton(onPressed: (){
        findBooks();
      }, child: const Text("Find books")),
    ],);
  }
}
