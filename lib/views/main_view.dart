import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sam/controllers/broswe_controller.dart';
import 'package:sam/views/browse_view.dart';
import 'package:sam/views/home_view.dart';
import 'package:sam/views/library_view.dart';

class MainView extends StatefulWidget {
  MainView({Key? key}) : super(key: key);

  
  _WidgetState createState() => _WidgetState();
}

class _WidgetState extends State<MainView> {

    @override
  void initState() {
    print("running children again");
    super.initState();
  }

  final List<Widget> _children = [
    const HomeView(),
    const LibraryView(),
    ChangeNotifierProvider<BrowseController>(create: (context) {
      print('calling notifier again');
      return BrowseController();
    }, builder: (context, child) {
      return BrowseView(
        controller: context.watch<BrowseController>(),
      );
    })
  ];

  int currentIndex = 0;

  tapping(int index) {
    setState(() {
      currentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _children[currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        onTap: tapping,
        selectedItemColor: Colors.red,
        currentIndex: currentIndex,
        items: const [
          BottomNavigationBarItem(icon: Icon(Icons.home), label: "Home"),
          BottomNavigationBarItem(
              icon: Icon(Icons.library_books), label: "Library"),
          BottomNavigationBarItem(
              icon: Icon(Icons.browse_gallery), label: "Browse"),
        ],
      ),
    );
  }
}
