
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pdfx/pdfx.dart';
import 'package:sam/components/topbar.dart';
import 'package:sam/database/books_db.dart';
import 'package:sam/database/session_db.dart';
import 'package:sam/models/book.dart';
import 'package:sam/models/session.dart';

class EBookView extends StatefulWidget {
  const EBookView(this.book,{Key? key}) : super(key: key);

  final Book book;

  _EBookViewState createState() => _EBookViewState();
}

class _EBookViewState extends State<EBookView> {
 late Future<PdfDocument> document;
 late PdfController controller;
 late Session session;
 int page=0;
 final numberController = TextEditingController();

  @override
  void initState() {
    super.initState();

    openDocument();
    session=Session(pageNumber: page, bookId: widget.book.id);
  }

  openDocument() async {
    page=widget.book.lastSession?.pageNumber??0;
    print("last page number is $page");
    document= PdfDocument.openFile(widget.book.path!);
    controller=PdfController(document: document, initialPage: page);
  }

  goBack(){
    Navigator.pop(context);
  }

  updateSession(){
            session.pageNumber=page;
            session.updatedAt=DateTime.now().toString();
            SessionDB().update(session);
            widget.book.updatedAt=DateTime.now().toString();
            BookDB().update(widget.book);
  }

  onPressed(){}

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: const Color(0xFFF5F7FB),
        appBar: AppBar(leading: GestureDetector(child: const Icon(Icons.arrow_back), onTap: (){
          goBack();
        },), title: Text(widget.book.title!, style: const TextStyle(fontSize: 14), overflow: TextOverflow.ellipsis,),),
        body: Column(children: [
          Expanded(child: PdfView(
            backgroundDecoration: const BoxDecoration(color: Colors.grey,),
            pageSnapping: true,
            controller: controller,onDocumentLoaded: (document) async{
            session.createdAt=DateTime.now().toString();
            session.startedAt=DateTime.now().toString(); 
            session.updatedAt=DateTime.now().toString();
            int? id=await SessionDB().insert(session);
            session.id=id;
          },onPageChanged: (p) {
            page=p;
            updateSession();
            setState(() {
              
            });
          },onDocumentError: (error) {
            print(error);
          },)),
          SizedBox(
            height: 50,
            child: Row(children:  [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: ElevatedButton(onPressed: (){
                  showAlert(context);
                }, child: const Text("Go to page")),
              ),
              Expanded(child: Container()),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: IconButton(onPressed: (){
                  controller.previousPage(duration: const Duration(seconds: 1), curve: Curves.linear);
                }, icon: const Icon(Icons.arrow_back_ios_rounded)),
              ),
              Text("$page"),const Text("/"),Text("${controller.pagesCount??'--'}"),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: IconButton(onPressed: (){
                  controller.nextPage(duration: const Duration(seconds: 1), curve: Curves.linear);
                }, icon: const Icon(Icons.arrow_forward_ios_rounded)),
              ),
            ],),
          ),
        ]),
      ),
    );
  }

  Future<void> showAlert(BuildContext context) async {
    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) {
        return AlertDialog(
          backgroundColor: const Color(0xFFF5F7FB),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15),
          ),
          contentPadding: const EdgeInsets.symmetric(vertical: 30, horizontal: 20),
          titlePadding: const EdgeInsets.only(top: 40, left: 20),
          actionsPadding:
              const EdgeInsets.symmetric(horizontal: 20.0, vertical: 30.0),
          actionsAlignment: MainAxisAlignment.spaceEvenly,
          title: const Text('Enter page number'),
          content: TextField(
            controller: numberController,
            keyboardType: TextInputType.number,
            inputFormatters: <TextInputFormatter>[
                FilteringTextInputFormatter.digitsOnly
            ], 
            maxLines: 1,
          ),
          actions: <Widget>[
            ElevatedButton(
              child: const Text('CANCEL'),
              style: buttonStyle(Colors.grey, 0.0),
              onPressed: () {
                Navigator.pop(context);
                numberController.clear();
              },
            ),
            ElevatedButton(
              style: buttonStyle(
                const Color(0xFF2E3092),
                0.0,
              ),
              child: const Text('Go'),
              onPressed: () {
                if(numberController.text==""){
                  return;
                }
                controller.jumpToPage(int.parse(numberController.text));
                Navigator.pop(context);
              },
            ),
          ],
        );
      },
    );
  }

  ButtonStyle buttonStyle(Color color, double elevation) {
    return ButtonStyle(
      elevation: MaterialStateProperty.all(elevation),
      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
        RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(5.0),
        ),
      ),
      padding: MaterialStateProperty.all<EdgeInsetsGeometry>(
        const EdgeInsets.symmetric(horizontal: 25, vertical: 10.0),
      ),
      backgroundColor: MaterialStateProperty.resolveWith<Color>(
        (Set<MaterialState> states) {
          return color; // Use the component's default.
        },
      ),
    );
  }


}