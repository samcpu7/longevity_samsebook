# Sam's Ebook

A Flutter project created with https://flutlab.io. Its to help me prove myself to the Longevity staff so I get hired.

## Getting Started

This project is a simple pdf reader. 

All it does is to load pdf files from the local device and display it for the reader to read. 
There are 3 main tabs. The home tab, the library tab and the browse tab. 
## Going deep

- The home tab display's the user's last read book. It also displays the last 5 most recently added books.
- The library tab is where all books the user have added to the app are displayed. Please note that user can add any pdf on the device to the app
- The browse tab is where all pdfs present on the user device are displayed. From here the user can add any book to the app by clicking on add to library button. 
When a pdf file is add to the app, it is stored in the apps local storage and becomes part of the library. The pdf will now show on the library tab and home tab. User can also add a pdf to the library by clicking on the "add to library" icon at he top right corner of the home and library screens. 

When a user clicks on a book to read, it opens at the last page the user was on. 
